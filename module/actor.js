/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class SimpleActor extends Actor {

  /** @override */
  getRollData() {
    const data = super.getRollData();
    // const shorthand = game.settings.get("mini-quest", "macroShorthand");

    // // Map all items data using their slugified names
    // data.items = this.data.items.reduce((obj, i) => {
    //   let key = i.name.slugify({strict: true});
    //   let itemData = duplicate(i.data);
    //   if ( !!shorthand ) {
    //     for ( let [k, v] of Object.entries(itemData.attributes) ) {
    //       if ( !(k in itemData) ) itemData[k] = v.value;
    //     }
    //     delete itemData["attributes"];
    //   }
    //   obj[key] = itemData;
    //   return obj;
    // }, {});
    return data;
  }

  /**
   * Apply a certain amount of damage or healing to the health pool for Actor
   * @param {number} amount       An amount of damage (positive) or healing (negative) to sustain
   * @param {number} multiplier   A multiplier which allows for resistance, vulnerability, or healing
   * @return {Promise<Actor>}     A Promise which resolves once the damage has been applied
   */
  async applyDamage(amount=0, multiplier=1) {
    let fullamount = Math.floor(parseInt(amount) * multiplier);
    const currHp = this.data.data.health.value || 0;
    const maxHp = this.data.data.health.max || 0;
    const ac = this.data.data.armorclass.value || 0;

    // fullamount < 0 => healing
    let newHealth = fullamount < 0 ?
      Math.min(currHp - fullamount, maxHp) :
      Math.max(currHp - fullamount + ac, 0);

    // Update the Actor
    const updates = {
      "data.health.value": newHealth
    };
    return this.update(updates);
  }
}
