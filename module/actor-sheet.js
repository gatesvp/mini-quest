/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SimpleActorSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["mini-quest", "sheet", "actor"],
  	  template: "systems/mini-quest/templates/actor-sheet.html",
      width: 600,
      height: 600,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();

    // Attributes code
    data.dtypes = ["String", "Number", "Boolean"];
    for ( let attr of Object.values(data.data.attributes) ) {
      attr.isCheckbox = attr.dtype === "Boolean";
    }

    // Data types
    data.data.weapons = this.actor.items.filter(item => item.type === "weapon");
    data.data.armors = this.actor.items.filter(item => item.type === "armor");
    data.data.spells = this.actor.items.filter(item => item.type === "spell");
    data.data.items = this.actor.items.filter(item => item.type === "item");

    return data;
  }

  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Add or Remove Attribute
    html.find(".attributes").on("click", ".attribute-control", this._onClickAttributeControl.bind(this));

    html.find('.item .item-image').click(event => this._onItemRoll(event));
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  _onItemRoll(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.getOwnedItem(itemId);

    if (item.type === "weapon") {
      let actorName = this.actor.data.name;
      let itemName = item.data.name;
      let damage = item.data.data["damage-dice"];
      let levelMod = (this.actor.data.data.level.value * 2) - 1;
      let targets = game.user.targets;

      if (targets.size > 1) {
        alert ("Select only one target");
        return;
      }
      if (targets.size <= 0) {
        alert ("Select a target");
        return;
      }

      let targetActorData = Array.from(targets)[0].actor.data
      let targetActorName = Array.from(targets)[0].data.name;

      let chatText = `<b>${actorName}</b> attacks <b>${targetActorName}</b> with ${itemName} for [[/r ${damage}+${levelMod}]]`;
      var messageData = {
        user: game.user._id,
        "speaker.actor": this.actor._id,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        content: chatText
      };

      const messageOptions = { };
      CONFIG.ChatMessage.entityClass.create(messageData, messageOptions);
    }
    if (item.type === "spell"){
      let actorName = this.actor.data.name;
      let itemName = item.data.name;
      let damage = item.data.data["damage-dice"];
      let numTargets = item.data.data["targets"];
      let mpCost = item.data.data.magic || 0;
      let actorMp = this.actor.data.data.magic.value;

      if (actorMp < mpCost) {
        alert ("not enough Magic remaining");
        return;
      }

      const updates = {
        'data.magic.value': actorMp - mpCost
      };
      this.actor.update(updates);

      let chatText = `${actorName} casts ${itemName} on ${numTargets} target for [[/r ${damage}]]`;
      var messageData = {
        user: game.user._id,
        "speaker.actor": this.actor._id,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        content: chatText
      };

      const messageOptions = { };
      CONFIG.ChatMessage.entityClass.create(messageData, messageOptions)
    }
    if (item.type === "item") {
      let actorName = this.actor.data.name;
      let itemName = item.data.name;
      let qty = item.data.data.quantity;
      let dieRoll = item.data.data["die-roll"];

      if (qty < 1) {
        alert ("you have used all of these");
        return;
      }

      let chatText = `${actorName} uses ${itemName}`;
      if (dieRoll) {
        chatText = `${actorName} uses ${itemName} for [[/r ${dieRoll}]]`;
      }

      const updates = {
        'data.quantity': qty - 1
      };
      item.update(updates);
      
      var messageData = {
        user: game.user._id,
        "speaker.actor": this.actor._id,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        content: chatText
      };

      const messageOptions = { };
      CONFIG.ChatMessage.entityClass.create(messageData, messageOptions)
    }

    //item.roll;
  }

  /* -------------------------------------------- */

  /**
   * Listen for click events on an attribute control to modify the composition of attributes in the sheet
   * @param {MouseEvent} event    The originating left click event
   * @private
   */
  async _onClickAttributeControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const attrs = this.object.data.data.attributes;
    const form = this.form;

    // Add new attribute
    if ( action === "create" ) {
      const nk = Object.keys(attrs).length + 1;
      let newKey = document.createElement("div");
      newKey.innerHTML = `<input type="text" name="data.attributes.attr${nk}.key" value="attr${nk}"/>`;
      newKey = newKey.children[0];
      form.appendChild(newKey);
      await this._onSubmit(event);
    }

    // Remove existing attribute
    else if ( action === "delete" ) {
      const li = a.closest(".attribute");
      li.parentElement.removeChild(li);
      await this._onSubmit(event);
    }
  }

  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) {

    // Handle the free-form attributes list
    const formAttrs = expandObject(formData).data.attributes || {};
    const attributes = Object.values(formAttrs).reduce((obj, v) => {
      let k = v["key"].trim();
      if ( /[\s\.]/.test(k) )  return ui.notifications.error("Attribute keys may not contain spaces or periods");
      delete v["key"];
      obj[k] = v;
      return obj;
    }, {});
    
    // Remove attributes which are no longer used
    for ( let k of Object.keys(this.object.data.data.attributes) ) {
      if ( !attributes.hasOwnProperty(k) ) attributes[`-=${k}`] = null;
    }

    // Re-combine formData
    formData = Object.entries(formData).filter(e => !e[0].startsWith("data.attributes")).reduce((obj, e) => {
      obj[e[0]] = e[1];
      return obj;
    }, {_id: this.object._id, "data.attributes": attributes});
    
    // Update the Actor
    return this.object.update(formData);
  }
}
